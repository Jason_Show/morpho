<%-- element-ui风格 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page isELIgnored="false"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fns" uri="http://www.max256.com/morpho/tags/functions" %>
<%--本文件是系统页面引用内容 如需个性化修改这里就可以--%>
<%	
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	String User_Agent = request.getHeader("User-Agent");
	out.println("<!--用户的浏览器信息： " + User_Agent + " -->");
%>
<%-- basePath --%>
<base href="<%=basePath%>" />
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!-- http元数据 -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!-- 解决部分浏览器不支持es6 promise特性-->
<script type="text/javascript" src="http://cdn.staticfile.org/es6-promise/4.1.1/es6-promise.auto.min.js" > </script>
<!-- 引入 Vue  七牛cdn速度快-->
<script type="text/javascript" src="http://cdn.staticfile.org/vue/2.5.21/vue.js"></script>
<!--  降级方案 备选cdn -->
<script type="text/javascript" >window.Vue || document.write(unescape("%3Cscript src='http://lib.baomitu.com/vue/2.5.21/vue.js' type='text/javascript'%3E%3C/script%3E"))</script>
<!-- 引入vant样式  jsdelivr cdn速度快-->
<link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/vant@1.4.8/lib/index.css" />
<!-- 引入vant组件 -->
<script type="text/javascript" src="http://cdn.jsdelivr.net/npm/vant@1.4.8/lib/vant.min.js"></script>
<!--  降级方案 备选cdn -->
<script type="text/javascript" >window.vant || document.write(unescape("%3Cscript src='http://unpkg.com/vant@1.4.8/lib/vant.min.js' type='text/javascript'%3E%3C/script%3E"))</script>
<!-- ajax库  七牛cdn速度快-->
<script type="text/javascript" src="http://cdn.staticfile.org/axios/0.18.0/axios.min.js"></script>
<!--  降级方案 备选cdn -->
<script type="text/javascript" >window.axios || document.write(unescape("%3Cscript src='http://unpkg.com/axios@0.18.0/dist/axios.min.js' type='text/javascript'%3E%3C/script%3E"))</script>
<!-- 表单提交辅助库  七牛cdn速度快 把js对象转为form表单形式-->
<script type="text/javascript" src="http://cdn.staticfile.org/qs/6.6.0/qs.min.js"></script>
<!--  降级方案 备选cdn -->
<script type="text/javascript" >window.Qs || document.write(unescape("%3Cscript src='http://lib.baomitu.com/qs/6.6.0/qs.min.js' type='text/javascript'%3E%3C/script%3E"))</script>
<!-- vConsole 微信公众号团队出品的 页面调试插件 -->
<script type="text/javascript" src="http://cdn.staticfile.org/vConsole/3.2.0/vconsole.min.js"></script>

<!-- jquery -->
<script src="${pageContext.request.contextPath}/statics/libs/jquery.min.js"></script>
<!-- json-->
<script src="${pageContext.request.contextPath}/jslib/jquery.json.min.js" type="text/javascript" charset="utf-8"></script>
<!-- js数据字典工具 -->
<script src="${pageContext.request.contextPath}/jslib/jquery.dicbinder.js" type="text/javascript" charset="utf-8"></script>
<!-- 前台时间格式化工具-->
<script src="${pageContext.request.contextPath}/jslib/jquery-dateFormat.js" type="text/javascript" charset="utf-8"></script>
<!-- 全局js -->
<script type="text/javascript" charset="utf-8">

	//vConsole 微信公众号团队出品的 页面调试插件  初始化  不new的话是不会显示调试按钮的 生产环境不需要请注释掉
	//var vConsole = new VConsole();

	//工具集合Tools
	window.T = {};
	// 获取请求参数
	// 使用示例
	// location.href = http://localhost:8080/index.html?id=123
	// T.p('id') --> 123;
	var urlParam = function(name) {
		var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if(r!=null)return  unescape(r[2]); return null;
	};
	T.urlParam = urlParam;
	T.p = urlParam;
	//工具js
	T.ctxPath='${ctx}';
    T.log=  function (info) {
        console.log(info);
    };
    
 	//jquery的全局的AJAX访问，处理AJAX请求时SESSION超时
	$.ajaxSetup({
		/* dataType: "json", */
		cache: false,
	    contentType:"application/x-www-form-urlencoded;charset=utf-8",
	    complete:function(XMLHttpRequest,textStatus){
	          if(XMLHttpRequest.status=="408"){
	               //跳转错误页 提示超时
	               window.location.replace('${ctx}/errorpage?shiroLoginFailure=0007');
	       		}	
	    }
	});
    
    var _axiosInstance = axios.create({
    	  timeout: 30000,/* 超时时间 */
    	  responseType: "json",/* 期待返回类型 */
    	  withCredentials: true, /* 是否允许带cookie这些 */
    	  headers: {
    		/* 请求头文件类型  */
    	    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"  
    	  }
    	});

    	/* POST传统表单参数序列化(添加请求拦截器) */
    	_axiosInstance.interceptors.request.use(
    	  function(config){
    	    // 在发送请求之前做某件事
    	    if (
    	      config.method === "post"
    	    ) {
    	      // Qs插件序列化为表单请求格式
    	      config.data = Qs.stringify(config.data);
    	      // 传统项目一般都需要进行此步,如果后台能直接接受json格式,可以不用 qs来序列化的
    	    }
    	    return config;
    	  },
    	  function(error){
    	    // error 的回调信息 本项目使用了vant
    	 	  vm.$dialog.alert({
    		  message: error && error.data.error.message
    		}).then(function() {
    		});
    	    return Promise.reject(error.data.error.message);
    	  }
    	);

    	/* 响应拦截器 */
    	_axiosInstance.interceptors.response.use(
    	  res => {
    	    //如果有响应数据但是业务状态status为0则表示响应是错误信息
    	    //这里业务错误仅仅是提示用户 如需更详细的业务失败处理方案 请不要使用这个
    	    if (res.data.status==='0') {
    	    	vm.$dialog.alert({
    	    		  /* 优先取res.data.info为空再取res.data.data */
    	    		  message: res.data.info? res.data.info : res.data.data
    	    		}).then(function() {
    	    		});
    	      	return Promise.reject(res.data.info? res.data.info : res.data.data);
    	    }
    	    return res;
    	  },
    	  function(error){
    		// 打日志
    		var errorInfo =  error.message ? error.message : error.response.data;
    	    console.info("服务器返回错误http状态码:"+error.response.status);
    	    console.info("服务器返回错误错误内容:"+errorInfo);
    	    console.info(error.response);
    		// 错误代码
    	    if (error.response.status === 405) {
    	    	vm.$dialog.alert({
    	   		  title: error.response.status+'错误',
    	   		  message: '不允许此方法对于请求所标识的资源，不允许使用请求行中所指定的方法。请确保为所请求的资源设置了正确的 MIME 类型。如果问题依然存在，请与服务器的管理员联系。'
    	   		}).then(function() {
    	   		});
    	    }
    	    else if (error.response.status === 500) {
    	    	vm.$dialog.alert({
    	    		title: error.response.status+'错误',
    	   		  message: '服务器的内部错误 Web服务器不能执行此请求。请稍后重试此请求。如果问题依然存在，请与 Web服务器的管理员联系。'
    	    	}).then(function() {
    	   		});
    	    }
    	    else if (error.response.status === 502) {
    	    	vm.$dialog.alert({
    	    		title: error.response.status+'错误',
    	   		  message: '网关出错,当用作网关或代理时，服务器将从试图实现此请求时所访问的upstream服务器中接收无效的响应。 如果问题依然存在，请与 Web服务器的管理员联HTTP常见错误'
    	    	}).then(function() {
    	   		});
    	    }
    	    else if (error.response.status === 404) {
    	    	vm.$dialog.alert({
    	    		title: error.response.status+'错误',
    	   		  message: 'Web服务器找不到您所请求的文件或脚本。请检查URL以确保路径正确。 如果问题依然存在，请与服务器的管理员联系。'
    	    	}).then(function() {
    	   		});
    	    }else{
    	    	/* 兜底 */
    	    	vm.$dialog.alert({
    	    		title: error.response.status+'错误',
    	   		  message: errorInfo
    	    	}).then(function() {
    	   		});
    	    }
    	    // 返回 response 里的错误信息
    	    return Promise.reject(errorInfo);
    	  }
    	);

    	//导出全局对象
    	var myajax = _axiosInstance;
</script>


